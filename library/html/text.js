/**
 * package: nodakwaeri
 * sub-package: html
 * author:  Richard B. Winters <a href='mailto:rik@mmogp.com'>Rik At MMOGP</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: Apache, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 */


// Some tools

var htmlTextTools =
{
    /**
     * Escapes special characters within a string in order to allow for storage
     * of said string within a database.
     *
     * @param s     string      The string to escape special characters in
     * @param o     object      An object containing optional paramters
     *
     * @returns     string      The supplied string with special characters escaped
     *
     * @since 0.4.0
     */
    addslashes: function( s, o )
    {
        var d =
        {
            regex:
            {
                backslashes: /\\/g,
                terminating: /\0/g,
                new_line: /\n/g,
                carriage_return: /\r/g,
                backspace: /[\b]/g,
                tab: /\t/g,
                percent: /\%/g,
                underscore: /\_/g,
                x1a: /\x1a/g,
                single_quote: /\'/g,
                double_quote: /\"/g
            },
            replace:
            {
                backslashes: '\\\\',
                terminating: '\\0',
                new_line: '\\n',
                carriage_return: '\\r',
                backspace: '\\b',
                tab: '\\t',
                percent: '\\%',
                underscore: '\\_',
                x1a: '\\Z',
                single_quote: '\\\'',
                double_quote: '\\"'
            }
        };

        if( !o || this.isEmptyObject( o ) )
        {
            o = d;
        }
        else
        {
            o.regex = this.extend( o.regex || {}, d.regex );
            o.replace = this.extend( o.replace || {}, d.replace );
        }

        console.log( 'r before addslashes: ' );
        console.log( s );
        var r = s;
        for( var regex in o.regex )
        {
            r = r.replace( o.regex[regex], o.replace[regex] );
        }

        console.log( 'r is: ' );
        console.log( r );

        return r;
    },

    /**
     * Strips newlines from strings stored in mysql, and replaces said new lines
     * with an html <br /> tag instead.
     *
     * @param s     string      Defines the string to execute this method on
     *
     * @returns     string      The supplied string with newlines replaced
     *
     * @since 0.4.0
     */
    nl2br: function( s )
    {
        var r = s;
        r = s.replace
        (
            /\n\r/g,
            function( x )
            {
                switch( x )
                {
                    default:
                    {
                        return ';<br />';
                    }
                }
            }
        );

        return s;
    },

    /**
     * A more granular than the default wrapper for splitting a string from user input into an array.
     *
     * @param s         String      Defines the non-javascript-object-formatted-string to turn into a json string
     * @param o         Object      Defines optional parameters
     *
     * @return      Array   Returns an array of elements which contain the text returned from the split method
     *
     * @since 0.4.0
     */
    split: function( s, o )
    {
        var d =
        {
            regex: /\s*,\s*/
        };
        o = this.extend( o || {}, d );

        if( o.hasOwnProperty( 'delimiter' ) )
        {
            o.regex = o.delimiter;
        }

        return s.split( o.regex );
    },

    stripslashes: function( s, o )
    {
        var d =
        {
            regex:
            {
                terminating: /\\0/g,
                new_line: /\\n/g,
                carriage_return: /\\r/g,
                backspace: /\\b/g,
                tab: /\\t/g,
                percent: /\\%/g,
                underscore: /\\_/g,
                x1a: /\\Z/g,
                single_quote: /\\\'/g,
                double_quote: /\\\"/g,
                backslashes: /\\\\/g
            },
            replace:
            {
                terminating: '\0',
                new_line: '\n',
                carriage_return: '\r',
                backspace: '\b',
                tab: '\t',
                percent: '%',
                underscore: '_',
                x1a: '\x1a',
                single_quote: '\'',
                double_quote: '\"',
                backslashes: '\\'
            }
        };

        if( !o || this.isEmptyObject( o ) )
        {
            o = d;
        }
        else
        {
            o.regex = this.extend( o.regex || {}, d.regex );
            o.replace = this.extend( o.replace || {}, d.replace );
        }

        console.log( 'r before stripslashes: ' );
        console.log( s );
        var r = s;
        for( var regex in o.regex )
        {
            r = r.replace( o.regex[regex], o.replace[regex] );
        }

        console.log( 'r is: ' );
        console.log( r );

        return r;
    }
};


//Export
module.exports = exports = htmlTextTools;