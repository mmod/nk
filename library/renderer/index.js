/**
 * package: nodakwaeri
 * sub-package: renderer
 * author:  Richard B. Winters <a href='mailto:rik@mmogp.com'>Rik At MMOGP</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: Apache, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 */


// Deps;
var fs = require( 'fs' );


/**
 * Entry point for the application renderer
 *
 * @param rtools    Rendering tools
 * @param dpath     Derived path to application views
 * @param xrm       Defines whether this application is an xrm
 * @param layout    Defines the derived path to the configured theme
 *
 * @since 0.4.0
 */
function renderer( rtools, dpath, xrm, layout, depath_base )
{
    this.nk = rtools
    this.rtools = new rtools.html( this.nk );
    this.dpath = dpath;
    this.xrm = xrm;
    this.layout = layout;
    this.dextension_base = depath_base;
    this.date = new Date();
};


/**
 * Fetches the layout content so it can be parsed
 *
 * @param request
 * @param response
 * @param klay
 *
 * @since 0.4.0
 */
renderer.prototype.turn = function( request, response, klay, isExtension, extData )
{
    // Prepare the derived view path and a copy of 'this' for later use;
    var vpath = this.dpath,
        epath = this.dextension_base,
        isExtensionCall = false,
        ectrlr = false,
        evendor = false,
        extension = false,
        eview = false,
        processor = this;

    // We'll need to modify paths if this is an extension call
    if( isExtension )
    {
        isExtensionCall = true;

        if( extData.hasOwnProperty( 'vendor' ) && extData.hasOwnProperty( 'extension' ) )
        {
            evendor = extData.vendor;
            extension = extData.extension;
        }
    }

    // We'll check if this application is an xrm
    if( this.xrm )
    {
        // If so we want to force the use of a layout
        // but only if it wasn't manually overridden.
        if( !klay.layoutOverride )
        {
            if( !klay.hasOwnProperty( 'layout' ) )
            {
                // If a layout wasn't specified, use the default
                // for the currently configured theme
                klay.layout = this.layout + '/main.kml';
            }
            else
            {
                // Otherwise, let's go ahead and use the specified
                // layout from the currently configured theme.
                if( !klay.layout || klay.layout === '' )
                {
                    // Set it to the default, as it was not overridden yet
                    // was set to false or an empty string.
                    klay.layout = this.layout + '/main.kml';
                }
                else
                {
                    // It contained a value, so prepare it accordingly.
                    klay.layout = this.layout + '/' + klay.layout + '.kml';
                }
            }

            // We also need to load any additionally required scripts, i.e. from
            // extensions and sorts

        }
        // If the value was overridden, we want to override the use of the
        // configured theme. Let's leave everything as it is, so that the
        // supplied value is not disturbed.
        //
        // If it was false, use of a layout will be skipped, if a string
        // value was supplied it will be used as the string path to the
        // specified layout.
    }

    // Check if a layout is specified after logic above has been executed, if so
    // we'll fetch said layout and process it for rendering the response.
    if( klay.layout )
    {
        fs.readFile
        (
            klay.layout,
            'utf8',
            function( error, ldata )
            {
                var buffer;

                // If there is an error
                if( error )
                {
                    // Log it
                    console.log( 'Error reading layout: ' + klay.layout );

                    // And set content to contain an error message
                    buffer = "<html><head><title>Failure</title></head><body><h1>Too Bad...</h1><p>There was an issue loading the requested layout!</p></body></html>";

                    response.statusCode = 200;
                    response.setHeader( 'Content-Type', 'text/html; charset=utf-8' );
                    response.setHeader( 'Content-Length', buffer.length );
                    response.write( buffer );
                    response.end();
                }
                else
                {
                    // Success
                    // There should always be a body
                    var derived_path = '';
                    if( isExtensionCall )
                    {
                        derived_path = epath + '/' + evendor + '/' + extension + '/views/' + klay.controller + '/' + klay.view + '.kml';
                    }
                    else
                    {
                        derived_path = vpath + '/' + klay.controller + '/' + klay.view + '.kml';
                    }

                    fs.readFile
                    (
                        derived_path,
                        'utf8',
                        function( error, vdata )
                        {
                            var buffer;

                            if( error )
                            {
                                // Log it
                                if( isExtensionCall )
                                {
                                    console.log( 'Error reading extension view, ' + derived_path );
                                }
                                else
                                {
                                    console.log( 'Error reading view, ' + derived_path );
                                }

                                // And set content to contain an error message
                                buffer = "<html><head><title>Failure</title></head><body><h1>Too Bad???...</h1><p>There was an issue loading the requested view!</p></body></html>";

                                response.statusCode = 200;
                                response.setHeader( 'Content-Type', 'text/html; charset=utf-8' );
                                response.setHeader( 'Content-Length', buffer.length );
                                //response.writeHead();
                                response.write ( buffer );
                                response.end();
                            }
                            else
                            {
                                //var pottr = nodakwaeri.klay();
                                //pottr.turn( request, response, klay );
                                klay.layout = ldata;
                                klay.view = vdata;

                                processor.shape( request, response, klay );
                            }
                        }
                    );
                }
            }
        );
    }else
    {
        // There should always be a body
        var derived_path = '';
        if( isExtensionCall )
        {
            derived_path = epath + '/' + evendor + '/' + extension + '/views/' + klay.controller + '/' + klay.view + '.kml';
        }
        else
        {
            derived_path = vpath + '/' + klay.controller + '/' + klay.view + '.kml';
        }

        fs.readFile
        (
            derived_path,
            'utf8',
            function( error, vdata )
            {
                var buffer;

                if( error )
                {
                    // Log it
                    if( isExtensionCall )
                    {
                        console.log( 'Error reading extension view, ' + derived_path );
                    }
                    else
                    {
                        console.log( 'Error reading view, ' + derived_path );
                    }

                    // And set content to contain an error message
                    buffer = "<html><head><title>Failure</title></head><body><h1>Too Bad???...</h1><p>There was an issue loading the requested view!</p></body></html>";

                    response.statusCode = 200;
                    response.setHeader( 'Content-Type', 'text/html; charset=utf-8' );
                    response.setHeader( 'Content-Length', buffer.length );
                    //response.writeHead();
                    response.write ( buffer );
                    response.end();
                }
                else
                {
                    //var pottr = nodakwaeri.klay();
                    //pottr.turn( request, response, klay );
                    klay.layout = false;
                    klay.view = vdata;

                    processor.shape( request, response, klay );
                }
            }
        );
    }
};


/**
 * Fetches a partial view so it can be parsed
 *
 * @param piece
 *
 * @since 2.5
 */
renderer.prototype.partial = function( piece )
{
    var processor = this;
    if( piece )
    {
        fs.readFile
        (
            this.dpath + '/' + piece + '.kml',
            'utf8',
            function( error, pdata )
            {
                var buffer;

                // If there is an error
                if( error )
                {
                    // Log it
                    console.log( 'Error reading partial: ' + klay.partial[p] + '.kml' );

                    // And set content to contain an error message
                    buffer = "<html><head><title>Failure</title></head><body><h1>Too Bad...</h1><p>There was an issue loading the requested layout!</p></body></html>";

                    response.statusCode = 200;
                    response.setHeader( 'Content-Type', 'text/html; charset=utf-8' );
                    response.setHeader( 'Content-Length', buffer.length );
                    response.write( buffer );
                    response.end();
                }
                else
                {
                    // Success
                    return pdata;
                }
            }
        );
    }
    return false;
};


/**
 * Pieces together - and sends - the response
 *
 * @param request
 * @param response
 * @param klay
 *
 * @since 0.0.1
 */
renderer.prototype.shape = function( request, response, klay )
{
    var processor = this;
    if( klay )
    {
        klay.authenticated = request.isAuthenticated;
    }

    // Now let's prepare the body content; there may not be a layout, but if there is we need the body first.
    var body = klay.view;
    body = this.parse( body, klay );

    // If a layout was requested, let's prep it
    if( klay.layout )
    {
        klay.view = body;
        var view = klay.layout;
        view = this.parse( view, klay );

        response.statusCode = 200;
        response.setSession();
        response.setHeader( 'Content-Type', 'text/html; charset=utf-8' );
        response.setHeader( 'Content-length', view.length );
        response.write( view );
        response.end();
    }
    else
    {
        response.statusCode = 200;
        response.setSession();
        response.setHeader( 'Content-Type', 'text/html; charset=utf-8' );
        response.setHeader( 'Content-length', body.length );
        response.write( body );
        response.end();
    }
};


/**
 * Parses kwaeri script
 *
 * @param content           Defines the content string to be parsed
 * @param klay              A reference to our klay object
 * @param iteration         Specifies which iteration is being invoked ( used for recursion )
 * @param identification    Specifies the identifier's name so we can properly reference it ( used for recursion )
 *
 * @since 1.0.0
 */
renderer.prototype.parse = function( content, klay, iteration, identification )
{
    var processor = this;

    if( !iteration )
    {
        iteration = false;
    }

    if( !identification )
    {
        identification = false;
    }

    // Prepare some objects for a more comprehensible experience
    var brackets = {},
        bracketsCount = {};

    // Fill em up
    brackets.opening = '[[';
    brackets.closing = ']]';
    brackets.positions = {};
    bracketsCount.opening = 0;
    brackets.positions.opening = [];
    bracketsCount.closing = 0;
    brackets.positions.closing = [];

    // First count the total number of opening brackets:
    var step = brackets.opening.length, // Should be the same length as closing
    pos = 0,
    run = true;

    while( run )
    {
        pos = content.indexOf( brackets.opening, pos );
        if( pos >= 0 )
        {
            brackets.positions.opening[bracketsCount.opening] = pos;
            bracketsCount.opening++;
            pos += step;
        }
        else
        {
            run = false;
        }
    }

    // Then count the total number of closing brackets:
    step = brackets.closing.length;
    pos = 0;
    run = true;

    while( run )
    {
        pos = content.indexOf( brackets.closing, pos );
        if( pos >= 0 )
        {
            brackets.positions.closing[bracketsCount.closing] = pos;
            bracketsCount.closing++;
            pos += step;
        }
        else
        {
            run = false;
        }
    }

    // Sort out the situation, log any pertinent information
    if( bracketsCount.opening > 0 && bracketsCount.closing > 0 )
    {
        if( bracketsCount.opening !== bracketsCount.closing )
        {
            if( bracketsCount.opening > bracketsCount.closing )
            {
                console.log( 'Possible non-closed subscript detected. Opening: ' + bracketsCount.opening + ' Closing: ' + bracketsCount.closing );
            }
            else
            {
                console.log( 'Possible non-opened subscript detected. Opening: ' + bracketsCount.opening + ' Closing: ' + bracketsCount.closing );
            }
        }
    }

    // Pull each top-level kwaeri block - which should contain all of its nested kwaeri blocks;
    // we will go in ascending numerical order to preserve any programmatic intent.
    var subscriptBlocks = [],
        count = 0;

    for( var i = 0; i < brackets.positions.opening.length; i++ )
    {
        // We need to be careful to check for nested kwaeri blocks, prepare
        // some logic for this.
        var offset = 1,
            offset2 = 0,
            run = true;

        while( run )
        {
            // If there are no additional brackets, the logic is pointless; continue
            if( ( i + offset ) > brackets.positions.opening.length )
            {
                break;
            }

            // Otherwise, check that the next opening bracket's position is actually greater than
            // the current closing bracket's position (if not, the current closing bracket is likely
            // the closing bracket of said next opening bracket)
            if( brackets.positions.opening[(i + offset)] < brackets.positions.closing[i + offset2] )
            {
                // And so we'll modify our logic to properly check for the matching closing bracket
                // position of the current opening bracket
                offset++;
                offset2++;
            }
            else
            {
                // And when we find the proper closing bracket, let us continue
                run = false;
            }
        }

        // We store the substring between the matching opening and closing brackets (which includes any nested kwaeri blocks)
        // so that we can do further testing. However, when we finally go with this new implementation we will be directly
        // replacing - or 'decorating' - the content of the string so as to properly format the response for the client.
        subscriptBlocks[count] = content.substr( brackets.positions.opening[i], ( ( brackets.positions.closing[(i + offset2)] + step ) - brackets.positions.opening[i] ) );

        // It's important we set the opening bracket's position we will use next, so that we are starting our next parse
        // after the current closing bracket's position.
        i += offset2;

        // And increment our separate count so that we properly extend the subscriptBlocks object with the next kwaeri block
        count++;
    }

    /* The following portions of code are for debugging purposes
    if( this.debug )
    {
        for( var i = 0; i < brackets.positions.opening.length; i ++ )
        {
            // View all of the bracket positions that were found, after the fact
            console.log( 'Opening bracket ' + i + ' position: ' + brackets.positions.opening[i] );
            console.log( 'Closing bracket ' + i + ' position: ' + brackets.positions.closing[i] );
        }

        // See how many we were able to pull out:
        //console.log( 'Kwaeri block identifiers counted: ' + bracketsCount.opening + ', ' + bracketsCount.closing );
        //console.log( 'Kwaeri subscript blocks parsed: ' + subscriptBlocks.length + ', ' + count );
    }
    */

    // Do further parsing, and decoration of each kwaeri block
    var o = content;
    for( var block in subscriptBlocks )
    {
        o = o.replace
        (
            subscriptBlocks[block],
            function()
            {
                // Prepare the subscriptBlock so our regex works properly
                subscriptBlocks[block] = subscriptBlocks[block].substr( step, ( subscriptBlocks[block].length - ( step * 2 ) ) );

                // First strip all comments
                subscriptBlocks[block] = subscriptBlocks[block].replace
                (   /[^:"](\/\/.*)[\r\n]|(\/\*([^]*?)\*\/)/mg,
                    function( match, singline, multiline, extra )
                    {
                        return "";
                    }
                );

                // Then handle the subscript
                subscriptBlocks[block] = subscriptBlocks[block].replace
                (   /^[\r\n\s]*(([\w_]+[\r\n\s]??)+?(\.[\w_]+[\r\n\s]*)*(\(([^]*?)\))?([\r\n\s]*(\{([^]*)\})?([\r\n\s]*else[\r\n\s]*\{([^]*)\})*?))/,
                    function( match, subscript, fof, memstring, argflow, args, ssflow, ssstring, superscript, ebflow, extrablock )
                    {
                        // Clear up any leading/trailing spaces in fof
                        fof = fof.replace( /^[\s\r\n]*|[\s\r\n]*$/g, '' );

                        console.log( 'Function or Factory: "' + fof + ( memstring || '' ) + '"' );

                        // Same with memstring if present
                        if( memstring )
                        {
                            memstring = memstring.replace( /^[\s\r\n]*|[\s\r\n]*$/g, '' );
                        }

                        // And same with args if present
                        if( args )
                        {
                            args = args.replace( /^[\s\r\n]*|[\s\r\n]*$/g, '' );
                        }

                        if( fof === 'partial' )
                        {
                            if( args )
                            {
                                var pdata = args;
                                pdata = pdata.replace( /^[\s\r\n]*|[\s\r\n]*$/g, '' );

                                return processor.parse( processor.partial( pdata ), klay );
                            }
                            return "";
                        }

                        if( fof === 'if' )
                        {
                            var argument = args.replace( /[\s\r\n]*/g, '' );

                            var pieces = ssflow.split( "else" );
                            if( pieces.length > 1 )
                            {
                                if( klay.viewbag.hasOwnProperty( argument ) || klay.hasOwnProperty( argument ) )
                                {
                                    var status;
                                    if( argument === 'authenticated' )
                                    {
                                        if( klay[argument] )
                                        {
                                            status = true;
                                        }
                                    }
                                    else
                                    {
                                        if( klay.viewbag[argument] )
                                        {
                                            status = true;
                                        }
                                    }

                                    if( status )
                                    {
                                        if( !pieces[0] )
                                        {
                                            return '';
                                        }

                                        pieces[0] = pieces[0].replace( /^[\s\r\n]*\{|\}?[\s\r\n]*$/g, '' );

                                        return processor.parse( pieces[0], klay );
                                    }
                                    else
                                    {
                                        if( !pieces[1] )
                                        {
                                            return '';
                                        }

                                        pieces[1] = pieces[1].replace( /^[\s\r\n]*\{|\}?[\s\r\n]*$/g, '' );

                                        return processor.parse( pieces[1], klay );
                                    }
                                }
                                else
                                {
                                    if( !pieces[1] )
                                    {
                                        return '';
                                    }

                                    pieces[1] = pieces[1].replace( /^[\s\r\n]*\{|\}?[\s\r\n]*$/g, '' );

                                    return processor.parse( pieces[1], klay );
                                }
                            }
                            else
                            {
                                if( klay.viewbag.hasOwnProperty( argument ) || klay.hasOwnProperty( argument ) )
                                {
                                    if( !pieces[0] )
                                    {
                                        return '';
                                    }

                                    var status;
                                    if( argument === 'authenticated' )
                                    {
                                        if( klay[argument] )
                                        {
                                            status = true;
                                        }
                                    }
                                    else
                                    {
                                        if( klay.viewbag[argument] )
                                        {
                                            status = true;
                                        }
                                    }

                                    if( !status )
                                    {
                                        return '';
                                    }

                                    pieces[0] = pieces[0].replace( /^[\s\r\n]*\{|\}?[\s\r\n]*$/g,'' );

                                    return processor.parse( pieces[0], klay );
                                }
                                else
                                {
                                    return '';
                                }

                            }

                            return 'Error: Could not find an associate code block for this [if] statement.';
                        }

                        if( fof === 'foreach' )
                        {
                            var found = false,
                            pieces = false,
                            iterator = false,
                            identifier = false;

                            console.log( 'foreach arguments: ' );
                            console.log( args );

                            if( args )
                            {
                                pieces = args.split( " as " );

                                if( pieces.length > 1 )
                                {
                                    // User has specified '..as <identifier>'
                                    iterator = pieces[0];
                                    identifier = pieces[1];
                                }
                                else
                                {
                                    iterator = pieces[0];
                                }

                                // The iterator may be a variable with property specifier
                                var iterator_pieces = iterator.split( '.' );
                                if( processor.nk.type( iterator_pieces ) === 'array' && iterator_pieces.length >= 1 )
                                {
                                    iterator = iterator_pieces;
                                }

                            }

                            // Strip all white space
                            if( identifier )
                            {
                                identifier = identifier.replace( /^[\s\r\n]*|[\s\r\n]*$/g, '' );
                            }

                            // Now, for each member of our array or object, we want to recursively run some scripts.  There could
                            // also be plain html, which should be copied and values parsed for each iteration.  Everything within
                            // superscript should be run/copied for each iteration, and should be passed only the member/record that
                            // the iteration pertains to.
                            var so = "";

                            // See if iterator is an array
                            if( processor.nk.type( iterator ) === 'array' && iterator.length > 0 )
                            {
                                if( klay.viewbag.hasOwnProperty( iterator[0] ) )
                                {
                                    var complexity = iterator.length;
                                    var puppet = false;
                                    var check_was_good = true;

                                    for( var i = 0; i < complexity; i++ )
                                    {
                                        if( check_was_good )
                                        {
                                            if( puppet.hasOwnProperty( iterator[i] ) )
                                            {
                                                puppet = puppet[iterator[i]];

                                                if( identifier )
                                                {
                                                    so += processor.parse( superscript, klay, puppet, identifier );
                                                }
                                                else
                                                {
                                                    so += processor.parse( superscript, klay, puppet, false );
                                                }
                                            }
                                            else
                                            {
                                                check_was_good = false;
                                            }
                                        }
                                        else
                                        {
                                            console.log( '[Renderer] Error: Reiterative check failed at current depth ' + i + ' of ' + complexity + ' total.' );
                                        }
                                    }

                                    return so;
                                }
                            }
                            else
                            {
                                if( klay.viewbag.hasOwnProperty( iterator ) )
                                {
                                    for( var record in klay.viewbag[iterator] )
                                    {
                                        // We pass the individual record as well as the entire contents of the foreach call, to be parsed
                                        // by the processor.  By passing the individual record, our processor will know to use the passed
                                        // record for values within
                                        if( identifier )
                                        {
                                            so += processor.parse( superscript, klay, klay.viewbag[iterator][record], identifier );
                                        }
                                        else
                                        {
                                            so += processor.parse( superscript, klay, klay.viewbag[iterator][record], false );
                                        }
                                    }

                                    return so;
                                }
                            }

                            return so;
                        }

                        if( fof === 'html' )
                        {
                            // Here we need to apply logic for filtering iterated parses
                            if( iteration  )
                            {
                                if( identification )
                                {
                                    return processor.decorate( fof, args, klay, memstring, iteration, identification );
                                }
                                else
                                {
                                    return processor.decorate( fof, args, klay, memstring, iteration, false );
                                }
                            }
                            else
                            {
                                return processor.decorate( fof, args, klay, memstring, false, false );
                            }
                        }

                        if( fof === 'modules' )
                        {
                            var position = args.replace( /[\s\r\n]*/g, '' );

                            if( klay.viewbag.hasOwnProperty( 'modules') || klay.hasOwnProperty( 'modules' ) )
                            {
                                var property = klay.viewbag.modules || klay.modules;
                                var status = false;
                                if( property.hasOwnProperty( position ) )
                                {
                                    status = true;
                                }

                                if( status )
                                {
                                    if( !ssflow )
                                    {
                                        return '';
                                    }

                                    var moduleContent = ssflow;
                                    moduleContent = moduleContent.replace
                                    (   /^[\s\r\n]*\{([^]*)\}[\s\r\n]*$/,
                                        function( match, capture )
                                        {
                                            return capture;
                                        }
                                    );

                                    return processor.parse( moduleContent, klay );
                                }
                                else
                                {
                                    if( !ssflow )
                                    {
                                        return '';
                                    }

                                    return ssflow;
                                }
                            }
                            else
                            {
                                if( !ssflow )
                                {
                                    return '';
                                }

                                return processor.parse( ssflow, klay );
                            }

                            return ssflow || '';
                        }

                        if( iteration )
                        {
                            if( identification )
                            {
                                return processor.decorate( ( fof + ( memstring || '' ) ), subscript, klay, "", iteration, identification );
                            }
                            else
                            {
                                return processor.decorate( ( fof + ( memstring || '' ) ), subscript, klay, "", iteration, false );
                            }
                        }
                        else
                        {
                            return processor.decorate( ( fof + ( memstring || '' ) ), subscript, klay, "", false, false );
                        }
                    }
                );

                return subscriptBlocks[block];
            }
        );
    }

    return o;
};


/**
 * Returns the result of a subscript
 *
 * @param fof
 * @param args
 * @param klay
 * @param memstring
 * @param iteration
 * @param identification
 *
 * @since 0.2.4
 */
renderer.prototype.decorate = function( fof, args, klay, memstring, iteration, identification )
{
    var processor = this;

    if( !iteration )
    {
        iteration = false;
    }

    if( !identification )
    {
        identification = false;
    }

    // Let's breakdown the function or factory
    var nested = false,
        buffered = '',
        fmembs = fof.split( '.' );
    if( fmembs.length > 1 )
    {
        nested = true;
        buffered = fmembs[0];
    }
    else
    {
        buffered = fof;
    }

    // Returns the output of the subscript
    switch( buffered )
    {
        case 'test':
        {
            return 'Test replacement worked!';
        }break;

        case 'title':
        {
            return klay.viewbag.title;
        }break;

        case 'links':
        {
            var o = '';
            if( klay.hasOwnProperty( 'links' ) )
            {
                if( klay.links.length > -1 )
                {
                    for( var i = 0; i < klay.links.length; i++ )
                    {
                        if( i === 0 )
                        {
                            o = klay.links[i] + '\n';
                        }
                        else
                        {
                            o += '        ' + klay.links[i] + '\n';
                        }
                    }
                }
            }
            return o;
        }break;

        case 'pagetitle':
        {
            return klay.viewbag.pagetitle;
        }break;

        case 'body':
        {
            return klay.view;
        }break;

        case 'scripts':
        {
            var o = '';
            if( klay.hasOwnProperty( 'scripts' ) )
            {
                if( klay.scripts.length > -1 )
                {
                    for( var i = 0; i < klay.scripts.length; i++ )
                    {
                        if( i === 0 )
                        {
                            o = klay.scripts[i] + '\n';
                        }
                        else
                        {
                            o += '        ' + klay.scripts[i] + '\n';
                        }
                    }
                }
            }
            return o;
        }break;

        case 'user':
        {
            // We can allow any depth:
            if( klay.viewbag.hasOwnProperty( 'user' ) )
            {
                var complexity = fmembs.length;
                var puppet = [];
                var check_was_good = true;

                // Build the puppet heiarchy
                for( var i = 1; i < complexity; i++ )
                {
                    if( check_was_good )
                    {
                        if( i === 1 )
                        {
                            if( klay.viewbag.user.hasOwnProperty( fmembs[i] ) )
                            {
                                puppet = klay.viewbag.user[fmembs[i]];
                            }
                            else
                            {
                                check_was_good = false;
                            }
                        }
                        else
                        {
                            if( puppet.hasOwnProperty( fmembs[i] ) )
                            {
                                puppet = puppet[fmembs[i]];
                            }
                            else
                            {
                                check_was_good = false;
                            }
                        }
                    }
                    else
                    {
                        console.log( '[Renderer] Error: Reiterative check failed at current depth ' + i + ' of ' + complexity + ' total.' );
                        break;
                    }
                }

                if( check_was_good )
                {
                    return puppet;
                }
            }
        }break;

        // This entire case will be pulled into an extension before long, as with much of this files functionality.
        case 'gravatar':
        {
            var user = '',
                email = '';

            var gravatar = true;
            if( klay.viewbag.hasOwnProperty( 'user' ) )
            {
                if( klay.viewbag.user.hasOwnProperty( 'username' ) )
                {
                    user = klay.viewbag.user.username;
                }
                else
                {
                    gravatar = false;
                    user = 'Guest';
                }

                if( klay.viewbag.user.hasOwnProperty( 'email' ) )
                {
                    email = klay.viewbag.user.email;
                }
                else
                {
                    email = "email@example.com";
                    gravatar = false;
                }
            }
            else
            {
                gravatar = false;
                user = 'guest';
                email = 'email@example.com';
            }

            var img_tag = '';
            if( gravatar )
            {
                var hash = '';
                hash = this.nk.hash_md5( email );
                var img_tag = '<img src="//www.gravatar.com/avatar/' + hash + '" alt="' + user + '" class="img-circle mmod-user-icon" />';
            }
            else
            {
                img_tag = '<img src="/images/default_user_icon.png" alt="' + user + '" class="img-circle mmod-user-icon" />';
            }

            return img_tag;

        }break;

        case 'date':
        {
            var o = '';
            switch( fmembs[1] )
            {
                case 'year':
                {
                    o = this.date.getFullYear();
                }break;
            }

            return this.date.getFullYear();
        }break;

        case 'model':
        {
            return 'model';
        }break;

        case 'html':
        {
            var rtools = processor.rtools,
            subscript = args,
            rargs = [],
            control = 0;

            // Strip our arguments
            subscript = subscript
            .replace    //(((([a-zA-Z0-9_]*)(\.([a-zA-Z0-9_]*))*(\((.*)\))?[\}]?)|([\{](.*)\}))[\,]?)*/mg,
            (   /(((([a-zA-Z0-9_]*)(\.([a-zA-Z0-9_]*))*(\((.*)\))?)|(\"\")|([\"](.*)[\"]?)[\,]?|([\{](.*)[\}]?))[\,]?)*/mg,
                function( match, match2, match3, noobjarg, method, memflow, member, argflow, argstring, objarg, objcontents  )
                {
                    if( match3 )
                    {
                        rargs[control] = match3;
                        control++;
                    }
                    return "";
                }
            );

            // The memstring contained .<whatever members were present after html>, let's split it
            var members = memstring.split( '.' );
            //console.log( members[0] + ' - ' + members[1] );

            // Make sure the rendering tools support the requested method
            if( rtools._classmap.hasOwnProperty( members[1] ) )
            {
                var memberparts = rargs[0].split( '.' );
                if( memberparts[0] === 'model' )
                {   // We'll grab the display text value from the model's schema
                    if( memberparts[1] )
                    {
                        if( klay.model.schema.hasOwnProperty( memberparts[1] ) )
                        {
                            rargs[0] = [ memberparts[1], klay.model ];
                        }
                        else
                        {
                            console.log( 'Error: Unknown member: `[Model].' + memberparts[1] + '`.' );
                            rargs[0] = [ false, 'Unknown member:  `[Model].' + memberparts[1] + '`.' ];
                        }
                    }
                    else
                    {
                        rargs[0] = [ false, 'Unknown member:  `[Model].' + memberparts[1] + '`.' ];
                    }
                }
                else
                {   // We'll literally pass what is requested from the viewbag
                    if( klay.viewbag )
                    {
                        var pcnt = 0;
                        if( this.nk.type( memberparts ) === 'array' )
                        {
                            pcnt = memberparts.length;
                        }

                        // We can support any depth here
                        if( pcnt == 0 )
                        {
                            if( klay.viewbag.hasOwnProperty( memberparts ) )
                            {
                                rargs[0] = [ memberparts, klay.viewbag[memberparts] ];
                            }
                            else
                            {
                                if( memberparts === identification && iteration )
                                {
                                    rargs[0] = [ memberparts, iteration ];
                                }
                                else
                                {
                                    // Text value
                                    rargs[0] = [ false, memberparts ];
                                }
                            }
                        }
                        else
                        {
                            if( klay.viewbag.hasOwnProperty( memberparts[0] ) )
                            {
                                var complexity = memberparts.length;
                                var puppet = klay.viewbag[memberparts[0]];
                                var current_property = memberparts[0];
                                var check_was_good = true;

                                for( var i = 1; i < complexity; i++ )
                                {
                                    if( check_was_good )
                                    {
                                        current_property = memberparts[i];
                                        if( puppet.hasOwnProperty( memberparts[i] ) )
                                        {
                                            puppet = puppet[memberparts[i]];
                                        }
                                        else
                                        {
                                            check_was_good = false;
                                        }
                                    }
                                    else
                                    {
                                        console.log( '[Renderer] Error: Reiterative check failed at current depth ' + i + ' of ' + complexity + ' total.' );
                                        rargs[0] = [ false, memberparts.join( '' ) ];
                                    }
                                }

                                rargs[0] = [ current_property, puppet ];
                            }
                            else
                            {
                                if( ( memberparts[0] === identification ) && iteration )
                                {
                                    var complexity = memberparts.length;
                                    var puppet = iteration;
                                    var current_property = identification;
                                    var check_was_good = true;

                                    for( var i = 1; i < complexity; i++ )
                                    {
                                        if( check_was_good )
                                        {
                                            current_property = memberparts[i];
                                            if( puppet.hasOwnProperty( memberparts[i] ) )
                                            {
                                                puppet = puppet[memberparts[i]];
                                            }
                                            else
                                            {
                                                check_was_good = false;
                                            }
                                        }
                                        else
                                        {
                                            console.log( '[Renderer] Error: Reiterative check failed at current depth ' + i + ' of ' + complexity + ' total.' );
                                            rargs[0] = [ false, memberparts.join( '' ) ];
                                        }
                                    }
                                    rargs[0] = [ current_property, puppet ];
                                }
                                else
                                {
                                    rargs[0] = [ false, memberparts[0] ];
                                }
                            }
                        }

                    }
                    else
                    {
                        // Text value
                        if( memberparts[0] )
                        {
                            rargs[0] = [ memberparts[0], memberparts[0] ];
                        }
                        else
                        {
                            rargs[0] = [ false, 'whoops' ];
                        }
                    }
                }

                // And finally we invoke the requested method and pass it any arguments that it needs to return a string
                if( rargs[0] && rargs[1] )
                {   // Two args provided, parse second argument as a JSON string representation of an object
                    rargs[1] = JSON.parse( rargs[1] );
                    subscript = rtools.generate( members[1], rargs[0], rargs[1] );
                }
                else if( rargs[0] )
                {   // One arg provided
                    subscript = rtools.generate( members[1], rargs[0] );
                }
                else
                {   // No args provided
                    subscript = rtools.generate( members[1] );
                }
            }
            else
            {
                // There just weren't any members which matched
                console.log( 'Error: Invalid script `html.' + members[1] + '`.' );
                subscript = "";
            }

            return String( subscript );
        }break;

        default:
        {
            if( iteration )
            {
                if( identification )
                {
                    if( fof )
                    {
                        // We're already iterating over a viewbag item, the identification let's us know what word was used to signify the current iteration.
                        var vbag = fof.split( "." );
                        if( vbag[0].toString == identification.toString )
                        {
                            if( iteration.hasOwnProperty( vbag[1] ) )
                            {
                                return iteration[vbag[1]];
                            }
                            else
                            {
                                return 'Does not exist: ' + iteration + '[' + vbag[1] + ']';
                            }
                        }
                    }

                    return '';
                }
                else
                {
                    return 'Error: Identification missing for this iteration of the invoked script provided in this kwaeri block.';
                }
            }
            else
            {
                if( fof )
                {
                    return klay.viewbag[fof];
                }

                return '';
            }
        }break;
    }

    // Enable this and disable the latter, for testing.
    //return 'Too bad...';
    return '';
};


//Export
module.exports = exports = renderer;